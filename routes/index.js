const express = require("express");
const router = express.Router();
const joi = require("joi");
const fs = require("fs/promises");

router.use(express.urlencoded({extended: true}));

/* GET home page. */
router.get("/", function (req, res, next) {
    res.render("index");
});

router.get("/wahl", function (req, res, next) {
    res.render("wahl", {
        listen: [{
            liste: "Nerdcampus",
            gremium: "den Senat",
            name: "senat_nerdcampus"
        }, {
            liste: "Nerdcampus",
            gremium: "das Studierendenparlament",
            name: "stupa_nerdcampus"
        }]
    });
});

router.get("/wahl_mids", function (req, res, next) {
    res.render("wahl_mids", {
        listen: [{
            liste: "Kreative Liste",
            gremium: "den Fakultätsrat",
            name: "fakrat_krea"
        }, {
            liste: "Kreative Liste",
            gremium: "das Fachschaftsparlament",
            name: "fsp_krea"
        }]
    });
});

const schema = joi.object().keys({
        firstname: joi.string().required(),
        lastname: joi.string().required(),
        faculty: joi.string().required(),
        birth: joi.string().required(),//'30.12.1995',
        matrikel: joi.string().pattern(/^[0-9]+$/).required(),//'21433604',
        str: joi.string().required(),
        plz: joi.string().pattern(/^[0-9]{5}$/).required(),
        ort: joi.string().required(),
        email: joi.string().email().required(),
        senat_nerdcampus: joi.string().valid("yes"),
        stupa_nerdcampus: joi.string().valid("yes"),
        fakrat_krea: joi.string().valid("yes"),
        fsp_krea: joi.string().valid("yes"),
        date: joi.string().required()
    }
);

router.post("/submit", function (req, res) {
    const formdata = {...req.body, date: (new Date()).toISOString().split("T")[0]}
    const inputValidationResult = schema.validate(formdata, {abortEarly: false});
    if (inputValidationResult.error) {
        const msg = inputValidationResult.error.details.map(err => err.message || err).join("\n");
        console.error(msg);
        res.render("submitError", {message: msg});
        return;
    }
    for (const list of ["senat_nerdcampus", "stupa_nerdcampus", "fakrat_krea", "fsp_krea"]) {
        if (formdata[list] === "yes") {
            const filename = `${__dirname}/../data/${list}.json.log`;
            console.error(`New Subscription: ${JSON.stringify(formdata)}.`)
            fs.appendFile(filename, JSON.stringify(formdata) + "\n");
        }
    }
    res.render("submitSuccess", {data: JSON.stringify(formdata, null, 4)});
});

router.get("/imprint", function (req, res, next) {
    res.render("imprint");
});

module.exports = router;
