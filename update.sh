#!/bin/bash
pushd /opt/website
git pull
yarn
sudo cp nerdcampus-website.service /etc/systemd/system/nerdcampus-website.service
sudo systemctl daemon-reload
sudo systemctl stop nerdcampus-website || true
sudo systemctl enable --now nerdcampus-website
popd
