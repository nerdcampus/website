const colors = require("tailwindcss/colors");

module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
        colors: {
            green: colors.lime,
            blue: colors.blue,
            primary: colors.lime
        }
    },
    variants: {
        extend: {},
    },
    plugins: [require("daisyui")],
};
